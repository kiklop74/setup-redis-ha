# Ubuntu specifics

## Prepare OS for redis setup

After OS install execute these commands


        sudo apt-get -q update
        sudo apt-get -yq upgrade
        sudo apt-get -yq install hugepages redis-server
        sudo systemctl stop ufw.service
        sudo systemctl disable ufw.service
        sudo reboot


## Disable transparent hugepage support on permanent basis

        sudo cp -uv disable-transparent-huge-page.service /lib/systemd/system/
        sudo systemctl daemon-reload
        sudo systemctl enable disable-transparent-huge-page.service
        sudo systemctl start disable-transparent-huge-page.service
        sudo systemctl restart redis-server.service

## Prepare client OS for benchmarking

## Ubuntu 18.04

        sudo apt install redis-tools
        echo 'fs.file-max=1000000' | sudo tee -a /etc/sysctl.conf
        echo '* - nofile 200000' | sudo tee -a /etc/security/limits.conf
        echo 'session required pam_limits.so' | sudo tee -a /etc/pam.d/common-session
        echo 'session required pam_limits.so' | sudo tee -a /etc/pam.d/common-session-noninteractive
        sudo sed -i 's/#DefaultLimitNOFILE=/DefaultLimitNOFILE=200000/g' /etc/systemd/system.conf
        sudo reboot

## Ubuntu 20.04

        sudo apt install redis-tools
        echo 'fs.file-max=1000000' | sudo tee -a /etc/sysctl.conf
        echo '* - nofile 200000' | sudo tee -a /etc/security/limits.conf
        sudo sed -i 's/#DefaultLimitNOFILE=/DefaultLimitNOFILE=200000/g' /etc/systemd/system.conf
        sudo reboot 
