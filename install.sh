#!/bin/bash

redisconfdir='/etc/redis'
redisconf="${redisconfdir}/redis.conf"
overrideconf="${redisconfdir}/override.conf"
escaped=$(echo "${overrideconf}" | sed 's/\//\\\//g')
# Get the 70% of total memory
memsize=$(( $(free -t | grep -oP '\d+' | sed '10!d') * 7 / 10 ))
hugepage=$(grep -P '\[never\]' /sys/kernel/mm/transparent_hugepage/enabled)
override=$(sudo grep -P "include ${escaped}" "${redisconf}")

# Copy the kernel optimizations
sudo cp -uv kernel-optimizations.conf /etc/sysctl.d/
# Copy the override configuration
sudo cp -uv override.conf "${redisconfdir}/"
# Set maxmemory to 70% of available RAM
echo "maxmemory ${memsize}" | sudo tee -a "${overrideconf}"

if [[ -z "${override}" ]]; then
    # Set override include to main redis configuration file
    echo "include ${overrideconf}" | sudo tee -a "${redisconf}"
fi

if [[ -z "${hugepage}" ]]; then
    # https://www.kernel.org/doc/html/latest/admin-guide/mm/transhuge.html
    sudo cp -uv disable-transparent-huge-page.service /lib/systemd/system/
    sudo systemctl daemon-reload
    sudo systemctl enable disable-transparent-huge-page.service
    sudo systemctl start disable-transparent-huge-page.service
fi

sudo systemctl restart redis-server.service
